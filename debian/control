Source: libgit2
Section: libs
Priority: optional
Maintainer: Utkarsh Gupta <utkarsh@debian.org>
Uploaders:
 Pirate Praveen <praveen@debian.org>,
 Mohammed Bilal <mdbilal@disroot.org>,
 Timo Röhling <roehling@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 ca-certificates,
 cmake,
 libhttp-parser-dev,
 libkrb5-dev,
 libmbedtls-dev,
 libpcre2-dev,
 libssh2-1-dev,
 pkgconf,
 python3-minimal:any,
 zlib1g-dev,
Standards-Version: 4.7.0
Homepage: https://libgit2.github.com/
Vcs-Git: https://salsa.debian.org/debian/libgit2.git
Vcs-Browser: https://salsa.debian.org/debian/libgit2
Rules-Requires-Root: no

Package: libgit2-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends:
 libgit2-1.9 (= ${binary:Version}),
 libhttp-parser-dev,
 libmbedtls-dev,
 libpcre2-dev,
 libssh2-1-dev,
 zlib1g-dev,
 ${misc:Depends},
Description: low-level Git library (development files)
 libgit2 is a portable, pure C implementation of the Git
 distributed version control system core methods provided as a
 re-entrant link-able library with a solid API.
 .
 This package contains the development files for libgit2.

Package: libgit2-1.9
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: low-level Git library
 libgit2 is a portable, pure C implementation of the Git
 distributed version control system core methods provided as a
 re-entrant link-able library with a solid API.

Package: libgit2-fixtures
Architecture: all
Multi-Arch: foreign
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
Description: low-level Git library - test suite examples
 libgit2 is a portable, pure C implementation of the Git
 distributed version control system core methods provided as a
 re-entrant link-able library with a solid API.
 .
 This package provides the test examples of the library, which
 can be useful for other pieces of software relying on libgit2,
 for testing purposes.
